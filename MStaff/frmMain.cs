﻿using AtsPlugin;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Linq;
using System.Media;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static HL.Bve;
using static System.Net.Mime.MediaTypeNames;
using static AtsPlugin.StaffEngine;

namespace CastPanel3
{
    internal class frmMain : Form
    {
        #region コントロール定義
        private PictureBox pbxCast;

        #endregion

        #region イニシャライズ
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.pbxCast = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCast)).BeginInit();
            this.SuspendLayout();
            // 
            // pbxCast
            // 
            this.pbxCast.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbxCast.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbxCast.Location = new System.Drawing.Point(0, 0);
            this.pbxCast.Name = "pbxCast";
            this.pbxCast.Size = new System.Drawing.Size(600, 800);
            this.pbxCast.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxCast.TabIndex = 0;
            this.pbxCast.TabStop = false;
            this.pbxCast.DoubleClick += new System.EventHandler(this.pbxCast_DoubleClick);
            this.pbxCast.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pbxCast_MouseClick);
            // 
            // frmMain
            // 
            this.ClientSize = new System.Drawing.Size(600, 800);
            this.Controls.Add(this.pbxCast);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MStaff";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxCast)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        #region 変数定義
        /// <summary>
        /// エンジン
        /// </summary>
        private AtsPlugin.StaffEngine staff;
        /// <summary>
        /// 設定
        /// </summary>
        private AtsPlugin.Config CFG;

        /// <summary>
        /// 車両状態
        /// </summary>
        private AtsEngine.MyVehicleState vehicleState;
        /// <summary>
        /// 
        /// </summary>
        //private bool isEnter = false;

        //private System.Media.SoundPlayer SoundPlayer = null;

        #region -通信関係
        private System.Net.Sockets.UdpClient udpClient = null;
        private int mPortNo = 49201;
        private bool mIsListening = false;
        #endregion

        #endregion

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public frmMain()
        {
            InitializeComponent();


        }

        #region イベント
        /// <summary>
        /// フォームロードイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {
                CFG = new Config();
                CFG.LoadStaions();

                staff = new AtsPlugin.StaffEngine();

                //音声用意
                //SoundPlayer = new System.Media.SoundPlayer(CastPanel2.Properties.Resources.CastCaution);

                // veshiStateを更新する
                this.vehicleState = new AtsEngine.MyVehicleState();

                // テストデータ
                //cast.mBveStaff = new HL.Staff();
                //cast.mBveStaff.StaffPack.StaffYomikomi("castimg\\station.txt");

                //cast.Init(cast.mBveStaff.StaffPack.Cars, null);
                //pbxCast.Image = cast.DrawCast(pbxCast.Size, 0, 0, 0, DateTime.Now.Hour * 3600 + DateTime.Now.Minute * 60 + DateTime.Now.Second);
                pbxCast.Image = MakeSampleImage(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// ファイルクローズ前イベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                //UdpClientを閉じる
                if (udpClient != null)
                {
                    udpClient.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #region ピクチャボックス
        private void pbxCast_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                int mX = (int)((double)e.X / (double)pbxCast.Width * 600.0); 
                int mY = (int)((double)e.Y / (double)pbxCast.Height * 800.0);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pbxCast_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if(this.WindowState != FormWindowState.Maximized)
                {
                    // 枠なし最大化して設定画面を出す
                    this.FormBorderStyle = FormBorderStyle.None;
                    this.WindowState = FormWindowState.Maximized;
                    using(frmConfig f = new frmConfig())
                    {
                        f.PortNo = this.mPortNo;
                        f.ListenStart = this.mIsListening;
                        f.ShowDialog();
                        this.mPortNo = f.PortNo;
                        if(this.mIsListening != f.ListenStart)
                        {
                            this.mIsListening = f.ListenStart;
                            if (f.ListenStart == true)
                            {
                                pbxCast.Image = MakeSampleImage(1);
                                // リッスンスタート
                                RecieveStart();
                            }
                            else
                            {
                                // 停止
                                if (this.udpClient != null)
                                {
                                    this.udpClient.Close();
                                }
                            }
                        }
                    }
                }
                else
                {
                    // 標準モードに戻す
                    this.FormBorderStyle = FormBorderStyle.Sizable;
                    this.WindowState = FormWindowState.Normal;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #endregion

        #region データ受信
        //データ受信の待機を開始する
        private void RecieveStart()
        {
            if (udpClient != null)
            {
                //Return
                udpClient.Close();
            }

            //UdpClientを作成し、指定したポート番号にバインドする
            System.Net.IPEndPoint localEP = new System.Net.IPEndPoint(System.Net.IPAddress.Any, (int)(this.mPortNo));
            udpClient = new System.Net.Sockets.UdpClient(localEP);
            // 非同期的なデータ受信を開始する
            udpClient.BeginReceive(new AsyncCallback(ReceiveCallback), udpClient);
        }

        //データを受信した時
        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                System.Net.Sockets.UdpClient udp = (System.Net.Sockets.UdpClient)(ar.AsyncState);

                //非同期受信を終了する
                System.Net.IPEndPoint remoteEP = null;
                byte[] rcvBytes;
                try
                {
                    rcvBytes = udp.EndReceive(ar, ref remoteEP);
                }
                catch (System.Net.Sockets.SocketException ex)
                {
                    Console.WriteLine("受信エラー({0}/{1})", ex.Message, ex.ErrorCode);
                    return;
                }
                catch //(ObjectDisposedException ex) 
                {
                    //すでに閉じている時は終了
                    Console.WriteLine("Socketは閉じられています。");
                    return;
                }


                //データを文字列に変換する
                string rcvMsg = System.Text.Encoding.UTF8.GetString(rcvBytes);
                rcvMsg = System.Uri.UnescapeDataString(rcvMsg);

                //受信したデータでイベント発生
                //Dim displayMsg As String = String.Format("[{0} ({1})] > {2}", remoteEP.Address, remoteEP.Port, rcvMsg)
                string displayMsg = String.Format("{0}", rcvMsg);
                pbxCast.BeginInvoke(new Action<string>(RenewCastPanel), displayMsg);
                Console.WriteLine(displayMsg);

                //再びデータ受信を開始する
                udp.BeginReceive(new AsyncCallback(ReceiveCallback), udp);


            }
            catch (Exception ex)
            {
                Console.Write("RC : " + ex.Message);
            }
        }

        #endregion


        #region 受信データ解析
        private void RenewCastPanel(string text)
        {
            ComSchema cs = null;
            try
            {
                if (text == "") return;

                //JSONをクラスに変換 ※クラスは出力したJSONを貼り付けて作成し、Listにデシリアライズする
                JsonSerializerOptions jso = new JsonSerializerOptions();
                jso.AllowTrailingCommas = true;
                jso.ReadCommentHandling = JsonCommentHandling.Skip;
                jso.NumberHandling = JsonNumberHandling.AllowReadingFromString | JsonNumberHandling.WriteAsString;
                jso.WriteIndented = true;
                jso.Encoder = JavaScriptEncoder.Create(System.Text.Unicode.UnicodeRanges.All);

                cs = JsonSerializer.Deserialize<ComSchema>(text, jso);

                if (cs == null)
                {

                }
                else
                {
                    if (cs.Beacon != null && cs.Beacon.Count() > 0)
                    {
                        SetBeaconData(cs);
                    }

                    if (cs.Stations != null)
                    {
                        // 駅リスト
                        bool mStaffAri = false;
                        HL.Bve.Station hbs = new HL.Bve.Station();
                        hbs.LoadFromData(cs.Stations.Replace("\\n", "\n"), ref mStaffAri);

                        if (hbs != null && hbs.StaffHeader != null)
                        {
                            this.staff.Init(hbs.StaffHeader.Cars, hbs);
                        }
                        else
                        {
                            this.staff.Init(2, hbs);
                        }
                    }
                    else
                    {
                        // フレーム

                        // veshiStateを更新する
                        AtsDefine.AtsVehicleState avs;
                        avs.BcPressure = 0;
                        avs.BpPressure = 0;
                        avs.Current = 0;
                        avs.ErPressure = 0;
                        avs.Location = cs.Location;
                        avs.MrPressure = 0;
                        avs.SapPressure = 0;
                        avs.Speed = (float)cs.Speed;
                        avs.Time = cs.Time;
                        vehicleState.SetState(avs);

                        // CAST画像を更新する                        
                        pbxCast.Image = staff.DrawStaff(pbxCast.Size, cs.Location, cs.Time, cs.StaId);
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message, "RenewCastPanel");
                Console.Write(ex.Message);
                //return null;
            }
        }

        #endregion

        #region SetBeaconData
        /// <summary>
        /// 地上子通過
        /// </summary>
        /// <param name="cs">受信データ</param>
        public void SetBeaconData(ComSchema cs)
        {
            try
            {
                if (cs.Beacon != null && cs.Beacon.Count() > 0)
                {
                    foreach (BeaconSchema beacon in cs.Beacon)
                    {
                        AtsDefine.AtsBeaconData beaconData;
                        beaconData.Type = beacon.Type;
                        beaconData.Distance = (float)beacon.Distance;
                        beaconData.Signal = beacon.Signal;
                        beaconData.Optional = beacon.Optional;

                        this.staff.SetBeaconData(vehicleState, beaconData);
                    }
                }
            }
            catch //(Exception ex)
            {
                //Log.Write("[Cast.SetBeaconData] : " + ex.Message);
            }
        }

        #endregion


        #region サンプル画像
        private Bitmap MakeSampleImage(int pMode)
        {
            Bitmap bmp = new Bitmap(600,800);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                if(pMode == 0)
                {
                    g.FillRectangle(Brushes.White, new RectangleF(0, 0, 600, 800));
                    HL.GraOpe.g_mojiInzi("表示エリアをダブルクリックして\n" +
                                         "設定画面を開いてください。", 
                                         g, new Font("MS ゴシック", 12), new Point(200, 350), new Size(200, 100), Brushes.Black, HL.GraOpe.ENUM_TextAlign.CenterMiddle);
                }
                else if(pMode == 1)
                {
                    g.FillRectangle(Brushes.White, new RectangleF(0, 0, 600, 800));
                    HL.GraOpe.g_mojiInzi("信号を待っています。\n" +
                                         "表示エリアをダブルクリックすると元のサイズに戻ります\n" +
                                         "終了する際は元のサイズに戻して右上のバツボタンを押してください。", 
                                         g, new Font("MS ゴシック", 12), new Point(200, 350), new Size(200, 100), Brushes.Black, HL.GraOpe.ENUM_TextAlign.CenterMiddle);
                }
            }

            return bmp;
        }
        #endregion
    }
}
