﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.IO;
using System.Text;
//using System.Text.Json;
//using System.Text.Json.Serialization;
//using System.Threading.Tasks;
using System.Xml.Linq;
using System.Windows.Forms;

using Zbx1425.DXDynamicTexture;
using HL;
using static System.Net.Mime.MediaTypeNames;
using System.Diagnostics.Eventing.Reader;
using static HL.Bve.Station;
using System.Runtime.Remoting.Contexts;
using System.Linq.Expressions;

namespace AtsPlugin
{
    /// <summary>
    /// 名鉄タブレットスタフもどきの画像を作るクラス
    /// </summary>
    public class StaffEngine : EngineBase
    {
        /// <summary>
        /// BVE停車場リスト
        /// </summary>
        public HL.Bve.Station StaffStation;

        private Bitmap StaffDisp;
        /// <summary>
        /// 時計以外のオブジェクト
        /// </summary>
        private Bitmap StaffBase;
        // - 時計
        private Bitmap ImgClock = null;

        private string mFontName = "メイリオ";

        //private DataTable RailwayTBL = null;
        private bool pNFB = false;
        private int pCars = 0;
        //private int pTmpLocation = 0;
        // 表示中駅インデックス
        private int mStationIndex = 0;
        private int mDeltaStaIndex = 0; // 停車場リストの0はスタフの何番目か
        private int mUnderCount = 0; // mDeltaStaIndex以下に表示すべき駅(行)の数
        private int mStartIndex = 0; // 先頭行のIndex
        // 停車駅番号
        //private int pStopIndex1 = 0;
        //private int pStopIndex2 = 0;

        // 駅リスト(駅一覧の駅番号, 駅名)
        //private Dictionary<int, string> Stations;

        // 送信用
        //private bool pStopFlag = false;
        private List<BeaconSchema> lBeacon;



        /// <summary>
        /// コンストラクタ
        /// </summary>
        public StaffEngine()
        {
            this.StaffDisp = new Bitmap(600, 800);

            // 別のフォントを使う場合はチェックしょり
            //try
            //{
            //    mFontName = "MS ゴシック";
            //    Font f = new Font(mFontName, 10);
            //    if (f.Name != mFontName)
            //    {
            //        // 存在しないと違うのになるらしい
            //        mFontName = "MS ゴシック";
            //    }
            //}
            //catch
            //{
            //    // これで落ちても困るのでcatch
            //    mFontName = "MS ゴシック";
            //}
            mFontName = "MS ゴシック";
        }

        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="cars">両数</param>
        /// <param name="sta">停車場リスト</param>
        public void Init(int cars, HL.Bve.Station sta)
        {
            int i;
            string logText = "";
            try
            {
                this.pNFB = true;
                //this.RailwayTBL.Clear();
                this.pCars = cars;
                if (sta != null)
                {
                    this.StaffStation = sta;
                }
                // 
                if(this.StaffStation != null)
                {
                    for (i = 0; i < this.StaffStation.StationList.Count; i++)
                    {
                        if (this.StaffStation.StationList[i] != null && this.StaffStation.StationList[i].Location != -1)
                        {
                            this.mDeltaStaIndex = i;
                            break;
                        }
                    }
                }

                if (System.IO.File.Exists(ModuleDirectoryPath + "\\clock.png") == true)
                {
                    System.Drawing.Image cb = System.Drawing.Image.FromFile(ModuleDirectoryPath + "\\clock.png");
                    this.ImgClock = new Bitmap(cb, cb.Size);
                }

                //毎回生成しなくていいもの
                logText = "Header";
                DrawBase();
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[Staff.Init]" + logText, false);
            }
        }

        public void Dispose()
        {
            if (this.StaffBase != null)
            {
                this.StaffBase.Dispose();
            }
            if (this.ImgClock != null)
            {
                this.ImgClock.Dispose();
            }

            this.StaffDisp.Dispose();
        }

        #region 描画

        public Bitmap DrawStaff(Size dispSize, int nowLocation, int pTime, int pStationIndex)
        {
            Bitmap rtn = new Bitmap(dispSize.Width, dispSize.Height);
            try
            {
                // 駅インデックス更新
                if (this.mStationIndex != pStationIndex + this.mDeltaStaIndex)
                {
                    this.mStationIndex = pStationIndex + this.mDeltaStaIndex;
                    DrawBase();
                }
                else
                {
                    this.mStationIndex = pStationIndex + this.mDeltaStaIndex;
                }
                // 下行数
                this.mUnderCount = 0;
                if(this.StaffStation != null)
                {
                    for (int i = mStationIndex; i < this.StaffStation.StationList.Count; i++)
                    {
                        if (this.StaffStation.StationList[i] != null)
                        {
                            if (this.StaffStation.StationList[i].Location != -1)
                            {
                                this.mUnderCount++;
                            }
                        }
                    }
                }
                // 
                if (this.mUnderCount < 16 && mStationIndex != 0)
                {
                    for (int i = mStationIndex - 1; i >= 0; i--)
                    {
                        if (this.StaffStation != null && this.StaffStation.StationList[i] != null)
                        {
                            if (this.StaffStation.StationList[i].Location != -1)
                            {
                                this.mUnderCount++;
                                if (this.mUnderCount >= 16)
                                {
                                    this.mStartIndex = i;
                                    break;
                                }
                            }
                        }
                        this.mStartIndex = i;
                    }
                }
                else
                {
                    this.mStartIndex = this.mStationIndex;
                }

                DrawClock(pTime);

                using(Graphics g = Graphics.FromImage(rtn))
                {
                    rtn = new Bitmap(this.StaffDisp, dispSize);
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[Staff.DrawStaff]", false);
            }
            return rtn;
        }

        #region ベース
        private void DrawBase()
        {
            string errormsg = "";
            Point pt;
            Size sz;
            Rectangle ra;
            try
            {
                if (this.StaffStation == null)
                {
                    return;
                }

                this.StaffBase = new Bitmap(600, 800);

                using (Graphics g = Graphics.FromImage(this.StaffBase))
                {
                    g.FillRectangle(Brushes.White, new Rectangle(0, 0, this.StaffBase.Width, this.StaffBase.Height));


                    // 
                    HL.GraOpe.g_mojiInzi("＜列車一覧", g, new Font(mFontName, 16), new Point(0, 24),
                                new Size(150, 32), Brushes.LightBlue, GraOpe.ENUM_TextAlign.LeftMiddle);

                    HL.GraOpe.g_mojiInzi("☀", g, new Font(mFontName, 14), new Point(400, 24),
                                         new Size(30, 30), Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);

                    HL.GraOpe.DrawArcRectangle(g, Brushes.Black, new Rectangle(430, 24, 48, 30), 4);
                    HL.GraOpe.g_mojiInzi("-", g, new Font(mFontName, 14), new Point(430, 24),
                                         new Size(48, 30), Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);

                    HL.GraOpe.FillArcRectangle(g, Brushes.Black, new Rectangle(482, 24, 48, 30), 4);
                    HL.GraOpe.FillArcRectangle(g, Brushes.White, new Rectangle(483, 25, 46, 28), 3);
                    HL.GraOpe.g_mojiInzi("+", g, new Font(mFontName, 14), new Point(482, 24),
                                         new Size(48, 30), Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);


                    // 乗務区
                    HL.GraOpe.g_mojiInzi(this.StaffStation.StaffHeader.Unyuku, g, new Font(mFontName, 14), new Point(135, 24),
                                new Size(171, 32), Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);

                    // 種別
                    string kind = "普通";
                    if (0 <= this.mStationIndex && this.mStationIndex < this.StaffStation.StationList.Count)
                    {
                        kind = this.StaffStation.StationList[this.mStationIndex].MStaffData.TrainKind;
                    }
                    pt = new Point(0, 56);
                    //sz = new Size(390, 72);
                    sz = new Size(295, 72);
                    ra = new Rectangle(new Point(0, 56), sz);
                    switch (kind)
                    {
                        case "普通": // 文字：白　背景：黒
                            g.FillRectangle(Brushes.Black, ra);
                            HL.GraOpe.g_mojiInzi(kind, g, new Font(mFontName, 40), pt, sz, Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);
                            break;
                        case "準急": // 文字：白　背景：緑
                            g.FillRectangle(Brushes.Green, ra);
                            HL.GraOpe.g_mojiInzi(kind, g, new Font(mFontName, 40), pt, sz, Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);
                            break;
                        case "急行": // 文字：白　背景：青
                            g.FillRectangle(Brushes.Blue, ra);
                            HL.GraOpe.g_mojiInzi(kind, g, new Font(mFontName, 40), pt, sz, Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);
                            break;
                        case "快急": // 文字：青　背景：白
                            HL.GraOpe.g_mojiInzi(kind, g, new Font(mFontName, 40), pt, sz, Brushes.Red, GraOpe.ENUM_TextAlign.CenterMiddle);
                            break;
                        case "特急": // 文字：白　背景：赤
                            g.FillRectangle(Brushes.Red, ra);
                            HL.GraOpe.g_mojiInzi(kind, g, new Font(mFontName, 40), pt, sz, Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);
                            break;
                        case "快特": // 文字：赤　背景：白
                            HL.GraOpe.g_mojiInzi(kind, g, new Font(mFontName, 40), pt, sz, Brushes.Red, GraOpe.ENUM_TextAlign.CenterMiddle);
                            break;
                        case "μＳ": // 文字：赤　背景：白
                            HL.GraOpe.g_mojiInzi(kind, g, new Font(mFontName, 40), pt, sz, Brushes.Red, GraOpe.ENUM_TextAlign.CenterMiddle);
                            break;

                        case "区快": // 文字：白　背景：緑
                            g.FillRectangle(Brushes.Green, ra);
                            HL.GraOpe.g_mojiInzi(kind, g, new Font(mFontName, 40), pt, sz, Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);
                            break;
                        case "快速": // 文字：白　背景：青
                            g.FillRectangle(Brushes.Blue, ra);
                            HL.GraOpe.g_mojiInzi(kind, g, new Font(mFontName, 40), pt, sz, Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);
                            break;
                        case "新快": // 文字：白　背景：赤
                            g.FillRectangle(Brushes.Red, ra);
                            HL.GraOpe.g_mojiInzi(kind, g, new Font(mFontName, 40), pt, sz, Brushes.White, GraOpe.ENUM_TextAlign.CenterMiddle);
                            break;
                        case "特快": // 文字：黒　背景：黄
                            g.FillRectangle(Brushes.Yellow, ra);
                            HL.GraOpe.g_mojiInzi(kind, g, new Font(mFontName, 40), pt, sz, Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                            break;

                        default: // 文字：黒　背景：白
                            HL.GraOpe.g_mojiInzi(kind, g, new Font(mFontName, 40), pt, sz, Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                            break;
                    }
                    g.DrawRectangle(new Pen(Brushes.Black, 1), ra);

                    // 車種
                    //ra = new Rectangle(new Point(390, 56), new Size(92, 72));
                    ra = new Rectangle(new Point(390, 56), new Size(105, 72));
                    HL.GraOpe.DrawString(this.StaffStation.StaffHeader.Keisiki, g, new Font(mFontName, 40), ra,
                                         Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                    g.DrawRectangle(new Pen(Brushes.Black, 1), ra);

                    // 両数
                    //HL.GraOpe.g_mojiInzi(this.pCars.ToString(), g, new Font(mFontName, 40), new Point(482, 56),
                    //            new Size(92, 72), Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                    //HL.GraOpe.g_mojiInzi("両", g, new Font(mFontName, 14), new Point(574, 56),
                    //            new Size(26, 72), Brushes.Black, GraOpe.ENUM_TextAlign.CenterBottom);
                    //g.DrawRectangle(new Pen(Brushes.Black, 1), new Rectangle(482, 56, 118, 72));
                    HL.GraOpe.g_mojiInzi(this.pCars.ToString(), g, new Font(mFontName, 40), new Point(295, 56),
                                new Size(95, 72), Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                    HL.GraOpe.g_mojiInzi("両", g, new Font(mFontName, 14), new Point(366, 56),
                                new Size(24, 72), Brushes.Black, GraOpe.ENUM_TextAlign.CenterBottom);
                    g.DrawRectangle(new Pen(Brushes.Black, 1), new Rectangle(295, 56, 95, 72));

                    // 通告
                    ra = new Rectangle(new Point(495, 56), new Size(105, 72));
                    HL.GraOpe.DrawString("通告", g, new Font(mFontName, 30), ra,
                                         Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                    g.DrawRectangle(new Pen(Brushes.Black, 1), ra);

                    // 始発駅
                    ra = new Rectangle(new Point(0, 128), new Size(30, 40));
                    HL.GraOpe.DrawString("発", g, new Font(mFontName, 14), ra,
                                         Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                    g.DrawRectangle(new Pen(Brushes.Black, 1), ra);

                    ra = new Rectangle(new Point(30, 128), new Size(105, 40));
                    HL.GraOpe.DrawString(this.StaffStation.StaffHeader.Shihatueki, g, new Font(mFontName, 24), ra,
                                         Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                    g.DrawRectangle(new Pen(Brushes.Black, 1), ra);

                    // 終着駅
                    ra = new Rectangle(new Point(0, 168), new Size(30, 40));
                    HL.GraOpe.DrawString("着", g, new Font(mFontName, 14), ra,
                                         Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                    g.DrawRectangle(new Pen(Brushes.Black, 1), ra);

                    ra = new Rectangle(new Point(30, 168), new Size(105, 40));
                    HL.GraOpe.DrawString(this.StaffStation.StaffHeader.Ikisaki, g, new Font(mFontName, 24), ra,
                                         Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                    g.DrawRectangle(new Pen(Brushes.Black, 1), ra);

                    // 列車番号
                    HL.GraOpe.g_mojiInzi(this.StaffStation.StaffHeader.TrainNumber, g, new Font(mFontName, 28), new Point(135, 128),
                                new Size(171, 80), Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                    HL.GraOpe.g_mojiInzi("列車", g, new Font(mFontName, 14), new Point(306, 128),
                                new Size(84, 80), Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);

                    g.DrawRectangle(new Pen(Brushes.Black, 1), new Rectangle(135, 128, 255, 80));

                    // 乗務終了
                    ra = new Rectangle(new Point(390, 128), new Size(210, 40));
                    g.FillRectangle(Brushes.LightGray, ra);
                    HL.GraOpe.DrawString("乗務終了", g, new Font(mFontName, 18), ra,
                                         Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                    g.DrawRectangle(new Pen(Brushes.Black, 1), ra);


                    // 時計
                    g.DrawRectangle(new Pen(Brushes.Black, 1), new Rectangle(390, 168, 210, 152));

                    // 記事
                    if(this.StaffStation.StaffHeader.Kiji != "")
                    {
                        ra = new Rectangle(new Point(390, 330), new Size(210, 26));
                        g.FillRectangle(Brushes.Gray, ra);
                        HL.GraOpe.DrawString("記事", g, new Font(mFontName, 14), ra,
                                             Brushes.Black, GraOpe.ENUM_TextAlign.LeftMiddle);
                        g.DrawRectangle(new Pen(Brushes.Black, 1), ra);

                        ra = new Rectangle(new Point(390, 356), new Size(210, 16));
                        ra = HL.GraOpe.DrawString(this.StaffStation.StaffHeader.Kiji, g, new Font(mFontName, 14), ra,
                                                  Brushes.Black, GraOpe.ENUM_TextAlign.LeftTop);
                        g.DrawRectangle(new Pen(Brushes.Black, 1), new Rectangle(390, 356, 210, (int)ra.Height + 16));
                    }

                    int cnt = 0;
                    bool mDI = false;
                    for (int i=this.mStartIndex; i < this.StaffStation.StationList.Count; i++)
                    {
                        OneStation oneStation = this.StaffStation.StationList[i];
                        // -1はスキップ
                        if (oneStation == null || oneStation.MStaffData.Index == -1) continue;
                        // 
                        int BaseTop = 208 + 37 * cnt; // 16行
                        // 前行表示
                        if(oneStation.MStaffData.BeforeText != "")
                        {
                            BaseTop += 37;
                            cnt++;
                            ra = new Rectangle(new Point(0, BaseTop - 37), new Size(306, 37));
                            g.FillRectangle(new SolidBrush(HL.StrOpe.Hex2Color(oneStation.MStaffData.BeforeBackColor)), ra);
                            HL.GraOpe.DrawString(oneStation.MStaffData.BeforeText, g, new Font(mFontName, 18), ra,
                                                 new SolidBrush(HL.StrOpe.Hex2Color(oneStation.MStaffData.BeforeForeColor)), 
                                                 GraOpe.ENUM_TextAlign.CenterMiddle);
                            g.DrawRectangle(new Pen(Brushes.Black, 1), ra);
                        }

                        // 駅名背景
                        if (oneStation.Location == -1)
                        {
                            // 乗務区間外
                            g.FillRectangle(Brushes.LightGray, new Rectangle(0, BaseTop, 306, 37));
                        }
                        else if (oneStation.MStaffData.ExtraStop == 1)
                        {
                            // 特別停車
                            g.FillRectangle(Brushes.Red, new Rectangle(0, BaseTop, 390, 37));
                        }
                        
                        if(this.mStationIndex <= i && oneStation.Location != -1 && mDI == false && oneStation.ArrivalTime.ToUpper() != "P")
                        {
                            // 次の駅
                            //g.FillRectangle(Brushes.Orange, new Rectangle(0, BaseTop, 306, 37));
                            // 次の停車駅
                            g.FillRectangle(Brushes.Orange, new Rectangle(0, BaseTop, 306, 37));
                            mDI = true;
                        }
                        //else if (oneStation.MStaffData.ExtraStop == 2)
                        //{
                        //    // 注意
                        //    g.FillRectangle(Brushes.Orange, new Rectangle(0, BaseTop, 306, 37));
                        //}

                        // 駅名
                        if (oneStation.ArrivalTime.ToUpper() == "P")
                        {
                            // 通過
                            HL.GraOpe.g_mojiInzi(oneStation.MStaffData.DispName, g, new Font(mFontName, 12), new Point(0, BaseTop),
                                        new Size(59, 37), Brushes.Black, GraOpe.ENUM_TextAlign.RightMiddle);
                        }
                        else
                        {
                            HL.GraOpe.g_mojiInzi(oneStation.MStaffData.DispName, g, new Font(mFontName, 18), new Point(0, BaseTop),
                                        new Size(60, 37), Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                        }
                        g.DrawRectangle(new Pen(Brushes.Black, 1), new Rectangle(0, BaseTop, 60, 37));

                        // 着時刻
                        if (oneStation.ArrivalTimeDate != DateTime.MinValue)
                        {
                            HL.GraOpe.g_mojiInzi(oneStation.ArrivalTimeDate.Hour.ToString("00") + ":" + oneStation.ArrivalTimeDate.Minute.ToString("00"),
                                        g, new Font(mFontName, 18), new Point(60, BaseTop),
                                        new Size(75, 37), Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);

                            ra = new Rectangle(new Point(143, BaseTop + 3), new Size(18, 18));
                            if (oneStation.ArrivalTimeDate.Second == 0)
                            {
                                g.FillRectangle(Brushes.Fuchsia, ra);
                            }
                            HL.GraOpe.DrawString(oneStation.ArrivalTimeDate.Second.ToString("00"),
                                                 g, new Font(mFontName, 12), ra,
                                                 Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                        }
                        g.DrawRectangle(new Pen(Brushes.Black, 1), new Rectangle(60, BaseTop, 107, 37));

                        // マーク
                        ra = new Rectangle(new Point(167, BaseTop), new Size(32, 37));
                        switch (oneStation.MStaffData.Mark)
                        {
                            case "X":
                                HL.GraOpe.DrawString("X", g, new Font(mFontName, 12), ra,
                                                     Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                                break;
                            case "OX":
                                HL.GraOpe.DrawString("○", g, new Font(mFontName, 12), ra,
                                                     Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                                HL.GraOpe.DrawString("X", g, new Font(mFontName, 12), ra,
                                                     Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                                break;
                            default:
                                break;
                        }
                        g.DrawRectangle(new Pen(Brushes.Black, 1), ra);

                        // 発時刻
                        if (oneStation.DepartureTimeDate != DateTime.MinValue)
                        {
                            HL.GraOpe.g_mojiInzi(oneStation.DepartureTimeDate.Hour.ToString("00") + ":" + oneStation.DepartureTimeDate.Minute.ToString("00"),
                                        g, new Font(mFontName, 18), new Point(199, BaseTop),
                                        new Size(75, 37), Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);

                            ra = new Rectangle(new Point(282, BaseTop + 3), new Size(18, 18));
                            if (oneStation.DepartureTimeDate.Second == 0)
                            {
                                g.FillRectangle(Brushes.Fuchsia, ra);
                            }
                            HL.GraOpe.DrawString(oneStation.DepartureTimeDate.Second.ToString("00"),
                                                 g, new Font(mFontName, 12), ra,
                                                 Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);
                        }
                        g.DrawRectangle(new Pen(Brushes.Black, 1), new Rectangle(199, BaseTop, 107, 37));

                        // 備考
                        HL.GraOpe.g_mojiInzi(oneStation.MStaffData.Etc, g, new Font(mFontName, 11), new Point(306, BaseTop),
                                    new Size(84, 37), Brushes.Black, GraOpe.ENUM_TextAlign.LeftBottom);

                        cnt++;
                    }
                    g.DrawRectangle(new Pen(Brushes.Black, 1), new Rectangle(306, 208, 84, 592));


                    // Graphicsオブジェクトのリソースを解放する
                    g.Dispose();
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[Cast.DrawStaff] : " + errormsg, false);
            }
        }
        #endregion

        private void DrawClock(int pTime)
        {
            string errormsg = "";
            Rectangle ra;
            try
            {
                if (this.pNFB == false || this.StaffBase == null)
                {
                    return;
                }

                using (Graphics g = Graphics.FromImage(this.StaffDisp))
                {
                    errormsg = "cBase";
                    g.DrawImage(this.StaffBase, new Point(0, 0));

                    // 現在日時
                    errormsg = "now";
                    ra = new Rectangle(new Point(0, 0), new Size(150, 10));
                    HL.GraOpe.DrawString(DateTime.Now.ToString("H:mm M月d日(ddd)"), g, new Font(mFontName, 6), ra,
                                         Brushes.Black, GraOpe.ENUM_TextAlign.LeftTop);

                    // バッテリー
                    errormsg = "bat";
                    // - バッテリーの充電状態を取得する
                    BatteryChargeStatus bcs = SystemInformation.PowerStatus.BatteryChargeStatus;
                    if (bcs == BatteryChargeStatus.Unknown)
                    {
                        //Console.WriteLine("不明です");
                    }
                    else
                    {
                        if ((bcs & BatteryChargeStatus.High) == BatteryChargeStatus.High)
                        {
                            //Console.WriteLine("充電レベルは、高い(66%より上)です");
                        }
                        if ((bcs & BatteryChargeStatus.Low) == BatteryChargeStatus.Low)
                        {
                            //Console.WriteLine("充電レベルは、低い(33%未満)です");
                        }
                        if ((bcs & BatteryChargeStatus.Critical) == BatteryChargeStatus.Critical)
                        {
                            //Console.WriteLine("充電レベルは、最低(5%未満)です");
                        }
                        if ((bcs & BatteryChargeStatus.Charging) == BatteryChargeStatus.Charging)
                        {
                            //Console.WriteLine("充電中です");
                        }
                        if ((bcs & BatteryChargeStatus.NoSystemBattery) == BatteryChargeStatus.NoSystemBattery)
                        {
                            //Console.WriteLine("バッテリーが存在しません");
                        }
                    }

                    // 回線
                    // GPS

                    //バッテリー残量（割合）
                    float blp = SystemInformation.PowerStatus.BatteryLifePercent;
                    //Console.WriteLine("バッテリー残量は、{0}%です", blp * 100);
                    ra = new Rectangle(new Point(540, 0), new Size(30, 10));
                    HL.GraOpe.DrawString("5G " + ((int)blp * 100).ToString() + "%", g, new Font(mFontName, 6), ra,
                                         Brushes.Black, GraOpe.ENUM_TextAlign.LeftTop);

                    //バッテリー残量（マーク）



                    // BVE内時刻
                    errormsg = "bveTime";
                    int hour = (int)(pTime / 3600) % 24;
                    int min = (int)((pTime % 3600) / 60);
                    int sec = pTime % 60;
                    double hRad = ((double)hour + (double)min / 60) * 30.0 * 2.0 * Math.PI / 360.0;
                    double mRad = ((double)min + (double)sec / 60) * 6.0 * 2.0 * Math.PI / 360.0;
                    double sRad = (double)sec * 6.0 * 2.0 * Math.PI / 360.0;

                    // 時針
                    errormsg = "clock";
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    if(this.ImgClock != null)
                    {
                        g.DrawImage(this.ImgClock, new Rectangle(435, 173, 120, 120));
                    }
                    g.DrawLine(new Pen(Brushes.Black, 2), new Point(495, 233), new Point(495 + (int)(Math.Sin(hRad) * 40), 233 - (int)(Math.Cos(hRad) * 40)));
                    g.DrawLine(new Pen(Brushes.Black, 2), new Point(495, 233), new Point(495 + (int)(Math.Sin(mRad) * 50), 233 - (int)(Math.Cos(mRad) * 50)));
                    g.DrawLine(new Pen(Brushes.Red, 1), new Point(495, 233), new Point(495 + (int)(Math.Sin(sRad) * 50), 233 - (int)(Math.Cos(sRad) * 50)));


                    // 時計
                    ra = new Rectangle(new Point(390, 298), new Size(210, 22));
                    HL.GraOpe.DrawString(hour.ToString("00") + " : " + min.ToString("00") + " : " + sec.ToString("00"),
                                         g, new Font(mFontName, 18), ra,
                                         Brushes.Black, GraOpe.ENUM_TextAlign.CenterMiddle);


                    // Graphicsオブジェクトのリソースを解放する
                    errormsg = "dispose";
                    g.Dispose();
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[Cast.DrawRailwayPanel]" + errormsg, false);
            }
        }
#endregion

        #region Elapse
        public void Elapse(AtsEngine.MyVehicleState vehicleState)
        {
            try
            {

            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[Staff.Elapse]", false);
            }
        }
        #endregion

        #region Door
        public void SetDoorOpen(bool open)
        {
            if (open == true)
            {

            }
            else
            {

            }
        }
        #endregion

        #region SetBeaconData
        /// <summary>
        /// 地上子通過
        /// </summary>
        /// <param name="vehicleState"></param>
        /// <param name="beaconData"></param>
        public void SetBeaconData(AtsEngine.MyVehicleState vehicleState, AtsDefine.AtsBeaconData beaconData)
        {
            try
            {
                // 選抜された地上子のみ来ている
                switch (beaconData.Type)
                {
                    case 85000:
                        // 初期化 (-1:完全初期化(停車場リストと通告) 0:部分初期化)
                        // 互換性のため残しておく
                        if(beaconData.Optional == -1)
                        {
                            Init(this.pCars, null);
                        }
                        else
                        {

                        }
                        break;
                    default:
                        break;
                }

                BeaconSchema beaconSchema = new BeaconSchema();
                beaconSchema.Type = beaconData.Type;
                beaconSchema.Distance = beaconData.Distance;
                beaconSchema.Signal = beaconData.Signal;
                beaconSchema.Optional = beaconData.Optional;
                if(lBeacon == null)
                {
                    lBeacon = new List<BeaconSchema>();
                }
                lBeacon.Add(beaconSchema);
            }
            catch (Exception ex)
            {
                Log.Write("[Cast.SetBeaconData] Type=" + beaconData.Type.ToString() + " : " + ex.Message);
            }
        }
        #endregion

        #region SetCars
        /// <summary>
        /// 両数をセット
        /// </summary>
        /// <param name="car"></param>
        public void SetCars(int car)
        {
            this.pCars = car;
            DrawBase();
        }
        #endregion

        #region 送信データ
        public string MakeComText(AtsEngine.MyVehicleState myVehicleState, int pStationIndex, List<CautionSchema> pCautionSchemas)
        {
            string retval = "";
            try
            {
                ComSchema comSchema = new ComSchema();
                comSchema.Time = (int)(myVehicleState.NewState.Time / 1000);
                comSchema.Location = (int)myVehicleState.NewState.Location;
                comSchema.PanelLocation = (int)Math.Abs(myVehicleState.Location);
                comSchema.Speed = Math.Abs(myVehicleState.NewState.Speed);
                comSchema.StaId = pStationIndex;

                if (this.lBeacon != null)
                {
                    comSchema.Beacon = lBeacon.ToArray();
                    lBeacon = null;
                }

                if (pCautionSchemas != null && pCautionSchemas.Count > 0)
                {
                    comSchema.Caution = pCautionSchemas.ToArray();
                    pCautionSchemas.Clear();
                }

                retval = MakeComText(comSchema);
            }
            catch (Exception ex)
            {
                Log.Write("[Cast.MakeComText] : " + ex.Message);
                retval = "";
            }
            return retval;
        }
        public string MakeComText(ComSchema comSchema)
        {
            // ※CastPanelとほぼ共通
            //JsonSerializer.Serialize(comSchema);
            string retval = "";
            string bsl = "";
            try
            {
                retval = "{";
                retval += "\"Time\":" + comSchema.Time.ToString() + ",";
                retval += "\"Location\":" + comSchema.Location.ToString() + ",";
                retval += "\"PanelLocation\":" + comSchema.PanelLocation.ToString() + ",";
                retval += "\"Speed\":" + comSchema.Speed.ToString("0.0") + ",";
                //retval += "\"Stop\":" + comSchema.StopStation.ToString() + ",";
                retval += "\"StaId\":" + comSchema.StaId.ToString() + "";

                if (comSchema.Beacon != null && comSchema.Beacon.Count() > 0)
                {
                    bsl = "";
                    retval += ",\"Beacon\":[";
                    foreach(BeaconSchema bs in comSchema.Beacon)
                    {
                        if (bsl != "") bsl += ",";
                        bsl += "{";
                        bsl += "\"Type\":" + bs.Type.ToString() + ",";
                        bsl += "\"Distance\":" + bs.Distance.ToString("0.0") + ",";
                        bsl += "\"Signal\":" + bs.Signal.ToString() + ",";
                        bsl += "\"Optional\":" + bs.Optional.ToString();
                        bsl += "}";
                    }
                    retval += bsl + "]";
                }

                if (comSchema.Caution != null && comSchema.Caution.Count() > 0)
                {
                    bsl = "";
                    retval += ",\"Caution\":[";
                    foreach (CautionSchema bs in comSchema.Caution)
                    {
                        if (bsl != "") bsl += ",";
                        bsl += "{";
                        bsl += "\"Number\":" + bs.Number.ToString() + ",";
                        bsl += "\"Type\":" + bs.Type.ToString() + ",";
                        bsl += "\"Caution\":\"" + bs.Caution + "\",";
                        bsl += "\"Location1\":" + bs.Location1.ToString("0.0") + ",";
                        bsl += "\"Location2\":" + bs.Location2.ToString("0.0") + ",";
                        bsl += "\"LimpSpeed\":" + bs.LimpSpeed.ToString() + "";
                        bsl += "}";
                    }
                    retval += bsl + "]";
                }

                retval += "}";
            }
            catch (Exception ex)
            {
                Log.Write("[Cast.MakeComText] : " + ex.Message);
                retval = "";
            }
            return retval;
        }
        #endregion

        #region 受信データ
        // このプラグインでは受信はしない
        #endregion
    }
}
