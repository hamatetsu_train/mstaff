﻿using AtsEx.PluginHost.Native;
using AtsEx.PluginHost.Panels.Native;
using AtsEx.PluginHost.Plugins;
using AtsEx.PluginHost.Sound.Native;
using AtsEx.PluginHost;
using BveTypes.ClassWrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace AtsPlugin
{
    [PluginAttribute(PluginType.VehiclePlugin)]
    public class AtsMain : AssemblyPluginBase
    {
        IntPtr panel;
        IntPtr sound;
        IAtsPanelValue<int>[] ExPanel = new IAtsPanelValue<int>[256];
        IAtsSound[] ExSound = new IAtsSound[256];

        public AtsMain(PluginBuilder services) : base(services)
        {
            InstanceStore.Initialize(Native, BveHacker);
            BveHacker.MainFormSource.Focus();

            BveHacker.MainFormSource.KeyDown += new KeyEventHandler(FrmMain_KeyDown);
            BveHacker.MainFormSource.KeyUp += new KeyEventHandler(FrmMain_KeyUp);
            Native.BeaconPassed += new AtsEx.PluginHost.Native.BeaconPassedEventHandler(BeaconPassed);
            Native.DoorClosed += new DoorEventHandler(DoorClosed);
            Native.DoorOpened += new DoorEventHandler(DoorOpened);
            Native.Started += new StartedEventHandler(Initialized);
            // Native.NativeKeys.AtsKeys[AtsEx.PluginHost.Input.Native.NativeAtsKeyName.A1].Pressed

            panel = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(int)) * 256);
            sound = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(int)) * 256);
            for (int i = 0; i < 256; i++)
            {
                //ExPanel[i] = Native.AtsPanelValues.RegisterInt32(i, 0, AtsEx.PluginHost.Binding.BindingMode.TwoWay);
                //ExSound[i] = Native.AtsSounds.Register(i);
                EngineBase.SetIndexValue(panel, i, 0);
                EngineBase.SetIndexValue(sound, i, 0);
            }

            if (AtsEngine.ATS == null)
            {
                AtsEngine.ATS = new AtsEngine();
            }
            AtsEngine.ATS.Load(BveHacker.ScenarioInfo.RouteFiles.SelectedFile.Path, BveHacker.ScenarioInfo.Path);
            AtsEngine.ATS.SetVehicleSpec(Ex2Def_VehicleSpec(Native.VehicleSpec));
            if (0 <= AtsEngine.ATS.CFG.Sound.NearStation && AtsEngine.ATS.CFG.Sound.NearStation <= 255)
            {
                ExSound[AtsEngine.ATS.CFG.Sound.NearStation] = Native.AtsSounds.Register(AtsEngine.ATS.CFG.Sound.NearStation);
            }

        }

        public override void Dispose()
        {
            BveHacker.MainFormSource.KeyDown -= new KeyEventHandler(FrmMain_KeyDown);
            BveHacker.MainFormSource.KeyUp -= new KeyEventHandler(FrmMain_KeyUp);
            Native.BeaconPassed -= new AtsEx.PluginHost.Native.BeaconPassedEventHandler(BeaconPassed);
            Native.DoorClosed -= new DoorEventHandler(DoorClosed);
            Native.DoorOpened -= new DoorEventHandler(DoorOpened);
            Native.Started -= new StartedEventHandler(Initialized);

            if (AtsEngine.ATS != null)
            {
                AtsEngine.ATS.Dispose();
            }
            AtsEngine.ATS = null;
        }

        public override TickResult Tick(TimeSpan elapsed)
        {
            // 次の駅のインデックス(たぶん戸閉すると加算される)
            int stationIndex = BveHacker.Scenario.Route.Stations.CurrentIndex + 1;
            AtsEngine.ATS.SetStationIndex(stationIndex);

            //var panelArray = new AtsDefine.AtsIoArray(panel);
            //var soundArray = new AtsDefine.AtsIoArray(sound);

            //AtsEngine.ATS.SetMS(BveHacker.Scenario.TimeManager.Time);

            AtsDefine.AtsVehicleState vehicleState = Ex2Def_VehicleState(Native.VehicleState);
            //Ex2Def_PanelSound();

            AtsDefine.AtsHandles outHandles = AtsEngine.ATS.Elapse(vehicleState, panel, sound);

            //Def2Ex_PanelSound();
            //if (0 <= AtsEngine.ATS.CFG.Sound.NearStation && AtsEngine.ATS.CFG.Sound.NearStation <= 255)
            //{
            //    // Sound
            //    int s = EngineBase.ReadIndexValue(sound, AtsEngine.ATS.CFG.Sound.NearStation);
            //    switch (s)
            //    {
            //        case AtsDefine.AtsSoundControlInstruction.Play:
            //            ExSound[AtsEngine.ATS.CFG.Sound.NearStation].Play();
            //            break;
            //        case AtsDefine.AtsSoundControlInstruction.Continue:
            //            break;
            //        case AtsDefine.AtsSoundControlInstruction.PlayLooping:
            //            ExSound[AtsEngine.ATS.CFG.Sound.NearStation].PlayLoop(0);
            //            break;
            //        case AtsDefine.AtsSoundControlInstruction.Stop:
            //            ExSound[AtsEngine.ATS.CFG.Sound.NearStation].Stop();
            //            break;
            //        default:
            //            ExSound[AtsEngine.ATS.CFG.Sound.NearStation].PlayLoop((double)EngineBase.ReadIndexValue(sound, AtsEngine.ATS.CFG.Sound.NearStation));
            //            break;
            //    }
            //}

            //AtsEngine.Log.Write("Weight=" + BveHacker.Scenario.Vehicle.Dynamics.FirstCar.Weight.ToString());
            //AtsEngine.Log.Write("Station=" + BveHacker.Scenario.Route.Stations.CurrentIndex.ToString());

            // return Def2Ex_OutHandles(outHandles);

            // 何もしないを返す
            //VehiclePluginTickResult tickResult = new VehiclePluginTickResult();
            //tickResult.HandleCommandSet = new AtsEx.PluginHost.Handles.HandleCommandSet(new AtsEx.PluginHost.Handles.NotchCommandBase.ContinueCommand(),
            //                                                                            new AtsEx.PluginHost.Handles.NotchCommandBase.ContinueCommand(),
            //                                                                            new AtsEx.PluginHost.Handles.ReverserPositionCommandBase.ContinueCommand(), 
            //                                                                            null);
            return new VehiclePluginTickResult(); ;
        }

        private void FrmMain_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            AtsEngine.ATS.FormKeyDown((int)e.KeyCode);
        }
        private void FrmMain_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            AtsEngine.ATS.FormKeyUp((int)e.KeyCode);
        }

        private void Initialized(StartedEventArgs e)
        {
            if (AtsEngine.ATS != null)
            {
                AtsEngine.ATS.Initialize((int)e.DefaultBrakePosition);
            }
        }

        private void DoorOpened(DoorEventArgs e)
        {
            AtsEngine.ATS.DoorOpen();
        }

        private void DoorClosed(DoorEventArgs e)
        {
            AtsEngine.ATS.DoorClose();
        }

        private void BeaconPassed(BeaconPassedEventArgs e)
        {
            AtsEngine.ATS.SetBeaconData(Ex2Def_Beacon(e));
        }


        #region 変換
        private AtsDefine.AtsVehicleSpec Ex2Def_VehicleSpec(AtsEx.PluginHost.Native.VehicleSpec _VehicleSpec)
        {
            AtsDefine.AtsVehicleSpec vehicleSpec;

            vehicleSpec.AtsNotch = _VehicleSpec.AtsNotch;
            vehicleSpec.B67Notch = _VehicleSpec.B67Notch;
            vehicleSpec.BrakeNotches = _VehicleSpec.BrakeNotches;
            vehicleSpec.Cars = _VehicleSpec.Cars;
            vehicleSpec.PowerNotches = _VehicleSpec.PowerNotches;

            return vehicleSpec;
        }

        private AtsDefine.AtsVehicleState Ex2Def_VehicleState(AtsEx.PluginHost.Native.VehicleState _VehicleState)
        {
            AtsDefine.AtsVehicleState vehicleState;

            vehicleState.BcPressure = _VehicleState.BcPressure;
            vehicleState.BpPressure = _VehicleState.BpPressure;
            vehicleState.Current = _VehicleState.Current;
            vehicleState.ErPressure = _VehicleState.ErPressure;
            vehicleState.Location = _VehicleState.Location;
            vehicleState.MrPressure = _VehicleState.MrPressure;
            vehicleState.SapPressure = _VehicleState.SapPressure;
            vehicleState.Speed = _VehicleState.Speed;
            vehicleState.Time = (int)(_VehicleState.Time.TotalMilliseconds);

            return vehicleState;
        }

        private VehiclePluginTickResult Def2Ex_OutHandles(AtsDefine.AtsHandles outHandles)
        {
            AtsEx.PluginHost.Handles.HandleSet handleSet = Native.Handles;

            AtsEx.PluginHost.Handles.NotchCommandBase powerCommand = handleSet.Power.GetCommandToSetNotchTo(outHandles.Power);
            AtsEx.PluginHost.Handles.NotchCommandBase brakeCommand = handleSet.Brake.GetCommandToSetNotchTo(outHandles.Brake);

            AtsEx.PluginHost.Handles.ReverserPositionCommandBase reverserCommand = AtsEx.PluginHost.Handles.ReverserPositionCommandBase.Continue;
            switch (outHandles.Reverser)
            {
                case -1:
                    reverserCommand = new AtsEx.PluginHost.Handles.ReverserPositionCommandBase.SetPositionCommand(ReverserPosition.B);
                    break;
                case 0:
                    reverserCommand = new AtsEx.PluginHost.Handles.ReverserPositionCommandBase.SetPositionCommand(ReverserPosition.N);
                    break;
                case 1:
                    reverserCommand = new AtsEx.PluginHost.Handles.ReverserPositionCommandBase.SetPositionCommand(ReverserPosition.F);
                    break;
            }

            AtsEx.PluginHost.Handles.ConstantSpeedCommand? constantSpeedCommand = AtsEx.PluginHost.Handles.ConstantSpeedCommand.Disable;
            switch (outHandles.ConstantSpeed)
            {
                case AtsDefine.AtsCscInstruction.Continue:
                    constantSpeedCommand = AtsEx.PluginHost.Handles.ConstantSpeedCommand.Continue;
                    break;
                case AtsDefine.AtsCscInstruction.Disable:
                    constantSpeedCommand = AtsEx.PluginHost.Handles.ConstantSpeedCommand.Disable;
                    break;
                case AtsDefine.AtsCscInstruction.Enable:
                    constantSpeedCommand = AtsEx.PluginHost.Handles.ConstantSpeedCommand.Enable;
                    break;
            }

            VehiclePluginTickResult vptr = new VehiclePluginTickResult();
            vptr.HandleCommandSet = new AtsEx.PluginHost.Handles.HandleCommandSet(powerCommand, brakeCommand, reverserCommand, constantSpeedCommand);
            return vptr;
        }

        private void Ex2Def_PanelSound()
        {
            for (int i = 0; i < 256; i++)
            {
                EngineBase.SetIndexValue(panel, i, ExPanel[i].Value);
                //EngineBase.SetPanelValue(sound, i, (int)ExSound[i].PlayState);
            }
        }

        private void Def2Ex_PanelSound()
        {
            for (int i = 0; i < 256; i++)
            {
                // Panel
                ExPanel[i].Value = EngineBase.ReadIndexValue(panel, i);

                // Sound
                int s = EngineBase.ReadIndexValue(panel, i);
                switch (s)
                {
                    case AtsDefine.AtsSoundControlInstruction.Play:
                        ExSound[i].Play();
                        break;
                    case AtsDefine.AtsSoundControlInstruction.Continue:
                        break;
                    case AtsDefine.AtsSoundControlInstruction.PlayLooping:
                        ExSound[i].PlayLoop(0);
                        break;
                    case AtsDefine.AtsSoundControlInstruction.Stop:
                        ExSound[i].Stop();
                        break;
                    default:
                        ExSound[i].PlayLoop((double)EngineBase.ReadIndexValue(sound, i));
                        break;
                }
            }
        }

        private AtsDefine.AtsBeaconData Ex2Def_Beacon(BeaconPassedEventArgs eBeacon)
        {
            AtsDefine.AtsBeaconData beaconData;
            beaconData.Type = eBeacon.Type;
            beaconData.Signal = eBeacon.SignalIndex;
            beaconData.Distance = eBeacon.Distance;
            beaconData.Optional = eBeacon.Optional;

            return beaconData;
        }
        #endregion
    }
}
