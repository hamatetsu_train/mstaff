﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtsPlugin
{
    /// <summary>
    /// JSON形式設定ファイル用スキーマ
    /// </summary>
    public class ConfigSchema
    {
        public Cfg_MStaff MStaff { get; set; }
        public Cfg_Sound Sound { get; set; }
        public Cfg_Com Com { get; set; }


        public class Cfg_MStaff
        {
            public string Size { get; set; }
            public string ImageName { get; set; }

            public bool Deformation { get; set; }

            public Cfg_DeformationPoint DeformationPoint { get; set; }
        }

        public class Cfg_DeformationPoint
        {
            public string TopLeft { get; set; }
            public string TopRight { get; set; }
            public string BottomRight { get; set; }
            public string BottomLeft { get; set; }
        }

        public class Cfg_Com
        {
            public bool UseCom { get; set; }
            public string HostName { get; set; }
            public int ToPort { get; set; }
        }

        public class Cfg_Sound
        {
            public int NearStation { get; set; }
        }
    }
}
